// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerCore.generated.h"

UCLASS()
class TOWERDEFENSE_API APlayerCore : public AActor
{
	GENERATED_BODY()
	
public:	

	// Sets default values for this actor's properties
	APlayerCore();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(BlueprintReadWrite)
		class APlayerHP* hp;

private:

	UFUNCTION()
		void OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void DecreaseHealth(class AEnemy* enemy);
public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
