// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveInfo.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveInfo : public UDataAsset
{
	GENERATED_BODY()

public:

		UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<TSubclassOf<class AEnemy>> EnemyArray;
};
