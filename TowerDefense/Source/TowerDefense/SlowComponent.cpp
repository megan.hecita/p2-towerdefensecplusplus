// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowComponent.h"
#include "Enemy.h"

USlowComponent::USlowComponent()
{

}

void USlowComponent::BeginPlay()
{
	Super::BeginPlay();
}

void USlowComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	
}

void USlowComponent::SetTarget(AActor* target)
{
	Super::SetTarget(target);

	if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
	{
		originalSpeed = enemy->speed;

		slowSpeed = enemy->speed / BuffEffectValue;
	}
}

void USlowComponent::ApplyBuff()
{
	if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
		enemy->ModifySpeed(slowSpeed);
}

void USlowComponent::RemoveBuff()
{
	if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
		enemy->ModifySpeed(originalSpeed);
}

void USlowComponent::ModifyBuffEffectValue(float amount)
{
	Super::ModifyBuffEffectValue(amount);
}
