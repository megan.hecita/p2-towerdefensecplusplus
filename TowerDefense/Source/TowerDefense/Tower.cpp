// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Projectile.h"
#include "Components/SphereComponent.h"
#include "Enemy.h"
#include "BuffComponent.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATower::RotateToTarget()
{
}

void ATower::DoAction()
{
}

void ATower::SetTarget()
{
}

void ATower::ChangeColour(bool canBuild)
{
	if (canBuild)
		SphereCollision->ShapeColor = FColor::Green;

	else
		SphereCollision->ShapeColor = FColor::Red;
}

void ATower::SetTowerMode(bool isTowerBuilt)
{
	if (isTowerBuilt)
		isBuilt = true;

	else
		isBuilt = false;
}

void ATower::TakeBuff(UBuffComponent* buff)
{
	curBuff = Cast<UBuffComponent>(buff);

	buff->ApplyBuff();
}

void ATower::SetAttackSpeed(float speed)
{
	if (speed <= 0)
		speed = 1;

	attackSpeed = speed;
}

void ATower::RemoveBuff()
{
	if (curBuff)
		curBuff->RemoveBuff();
}

void ATower::UpgradeTower(float amount)
{
	effectValue += amount;
	level += 1;
}

void ATower::UpgradePrice()
{
	price += 100;
}

void ATower::GhostTowerMode()
{
	SphereCollision->SetHiddenInGame(false);
}

void ATower::BuiltTowerMode()
{
	SphereCollision->SetHiddenInGame(true);
	isBuilt = true;
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isBuilt)
	{
		BuiltTowerMode();
		curTime += 1 * DeltaTime;

		if (curTime >= attackSpeed)
		{
			canFire = true;
		}
	}

	else
	{
		GhostTowerMode();
	}
}

