// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCore.h"
#include "Engine/StaticMesh.h"
#include "Enemy.h"
#include "PlayerHP.h"

// Sets default values
APlayerCore::APlayerCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &APlayerCore::OnHit);
}

// Called when the game starts or when spawned
void APlayerCore::BeginPlay()
{
	Super::BeginPlay();
}

void APlayerCore::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		DecreaseHealth(target);
		target->Destroy();
	}
}

void APlayerCore::DecreaseHealth(AEnemy* enemy)
{
	if (hp)
		hp->DecreaseHealth(enemy->damage);
}

// Called every frame
void APlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



