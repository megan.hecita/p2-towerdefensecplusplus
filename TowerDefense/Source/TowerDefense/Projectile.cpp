// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Enemy.h"
#include "Components/SphereComponent.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectile::DestroySelf()
{
	this->Destroy();
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	curTime += 1 * DeltaTime;

	if (curTime >= lifetime)
	{
		DestroySelf();
	}
}

void AProjectile::SetDamage(int amount)
{
	damage = amount;
}

void AProjectile::SetBuff(UBuffComponent* Buff)
{
	BuffEffect = Buff;
}

void AProjectile::UpgradeSphereRadius(float upSize)
{
}
