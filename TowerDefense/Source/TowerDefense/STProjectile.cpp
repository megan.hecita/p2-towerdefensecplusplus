// Fill out your copyright notice in the Description page of Project Settings.


#include "STProjectile.h"
#include "Enemy.h"
#include "Components/SphereComponent.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/ProjectileMovementComponent.h"

ASTProjectile::ASTProjectile()
{
	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	AddOwnedComponent(ProjectileMovement);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ASTProjectile::DoDamage);
}

void ASTProjectile::DoDamage(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		target->TakeDamage(damage);

		DestroySelf();
	}
}

void ASTProjectile::DestroySelf()
{
	Super::DestroySelf();
}

void ASTProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASTProjectile::SetDamage(int amount)
{
	Super::SetDamage(amount);
}
