// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "Salvager.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASalvager : public ATower
{
	GENERATED_BODY()
	
public:

	ASalvager();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerWallet* wallet;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ReceiveGoldTime;

	UPROPERTY()
		float currentTime;

private:

	UFUNCTION()
		void LookForTarget(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	virtual void BeginPlay() override;
	
	void DoAction() override;

	void Tick(float DeltaTime) override;

	void SetTarget() override;
	
	void GhostTowerMode() override;

	void BuiltTowerMode() override;

	UFUNCTION()
		void GiveCoins();

public:

	void ChangeColour(bool canBuild) override;

	void SetTowerMode(bool isTowerBuilt) override;

	void UpgradeTower(float amount) override;
};
