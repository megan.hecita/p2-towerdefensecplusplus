// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerWallet.h"
#include "Enemy.h"
// Sets default values
APlayerWallet::APlayerWallet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerWallet::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerWallet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerWallet::ReceiveGold(int32 amount)
{
	gold += amount;
}

void APlayerWallet::DecreaseGold(int32 amount)
{
	gold -= amount;

	if (gold < 0)
		gold = 0;
}

