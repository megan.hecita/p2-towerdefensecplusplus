// Fill out your copyright notice in the Description page of Project Settings.


#include "MissileProjectile.h"
#include "Enemy.h"
#include "Components/SphereComponent.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/ProjectileMovementComponent.h"

AMissileProjectile::AMissileProjectile()
{
	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AMissileProjectile::OnHitTrigger);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	AddOwnedComponent(ProjectileMovement);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AMissileProjectile::DoDamage);
	SphereCollision->Deactivate();
}

void AMissileProjectile::DoDamage(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if (isFlyingIgnored)
		{
			if (!target->isFlying)
				target->TakeDamage(damage);
		}
	}
}

void AMissileProjectile::DestroySelf()
{
	Super::DestroySelf();
}


void AMissileProjectile::OnHitTrigger(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		SphereCollision->Activate();
		ProjectileMovement->Deactivate();
		StaticMesh->SetVisibility(false);
	}
}

void AMissileProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMissileProjectile::SetDamage(int amount)
{
	Super::SetDamage(amount);
}

void AMissileProjectile::UpgradeSphereRadius(float upSize)
{
	float originalRad = SphereCollision->GetScaledSphereRadius();

	float newRad = originalRad + upSize;

	SphereCollision->SetSphereRadius(newRad);
}
