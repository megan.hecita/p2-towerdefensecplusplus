// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"
#include "Waypoint.h"
#include "WaveInfo.h"
#include "Kismet/KismetMathLibrary.h"
#include "Enemy.h"
#include "TowerDefenseGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerWallet.h"

// Sets default values
AEnemySpawner::AEnemySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
}

void AEnemySpawner::SpawnEnemies(int32 index)
{
	FVector spawnLocation = this->GetActorLocation();

	AEnemy* enemySpawned = GetWorld()->SpawnActor<AEnemy>(WaveArray->EnemyArray[index], FTransform(spawnLocation));

	TArray<AWaypoint*> path = GetRandomPath();

	enemySpawned->SetHealthAndReward(hpBuff, goldBuff);

	enemySpawned->SetPath(path, startingPoint);

	ATowerDefenseGameMode* gameMode = GetWorld()->GetAuthGameMode<ATowerDefenseGameMode>();

	enemySpawned->OnDeath.AddDynamic(gameMode, &ATowerDefenseGameMode::AddEnemyDestroyed);
	enemySpawned->OnKilled.AddDynamic(playerWallet, &APlayerWallet::ReceiveGold);
}

void AEnemySpawner::AddBuff()
{
	hpBuff += 5;
	goldBuff += 10;
}

TArray<class AWaypoint*> AEnemySpawner::GetRandomPath()
{
	if (pathNum == 0)
	{
		startingPoint = 1;

		return WaypointPathA;
	}

	else
	{
		startingPoint = 5;

		return WaypointPathB;
	}
}

// Called every frame
void AEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

