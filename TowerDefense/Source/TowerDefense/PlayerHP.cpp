// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHP.h"

// Sets default values
APlayerHP::APlayerHP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerHP::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerHP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerHP::DecreaseHealth(float damage)
{
	hp -= damage;

	if (hp <= 0)
		ShowGameOverScreen();
}

