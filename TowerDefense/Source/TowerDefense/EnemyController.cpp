// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyController.h"
#include "Enemy.h"


AEnemyController::AEnemyController()
{

}

void AEnemyController::BeginPlay()
{
	Super::BeginPlay();
}

void AEnemyController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	AEnemy* Enemy = Cast<AEnemy>(GetPawn());

	if (Enemy)
		Enemy->MoveToWaypoints();
}


