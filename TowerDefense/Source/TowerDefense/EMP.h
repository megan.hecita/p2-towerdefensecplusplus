// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "EMP.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AEMP : public ATower
{
	GENERATED_BODY()
	
public:
	
	AEMP();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* StaticMesh;

private:

	UFUNCTION()
		void StartBuff(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void EndBuff(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void DoAction() override;

	void Tick(float DeltaTime) override;

	void SetTarget() override;

	void GhostTowerMode() override;

	void BuiltTowerMode() override;

public:

	void ChangeColour(bool canBuild) override;

	void SetTowerMode(bool isTowerBuilt) override;

	UFUNCTION()
		void OnRemoveTower(AActor* actor);

	void UpgradeTower(float amount) override;
};
