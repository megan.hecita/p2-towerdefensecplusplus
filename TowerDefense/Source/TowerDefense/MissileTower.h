// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "MissileTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMissileTower : public ATower
{
	GENERATED_BODY()

public:
	
	AMissileTower();

protected:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* StaticMeshStand;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* StaticMeshTurret; 

private:

	UFUNCTION()
		void LookForTarget(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void ChangeTarget(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void DoAction() override;

	void RotateToTarget() override;

	void Tick(float DeltaTime) override;

	void SetTarget() override;

	void GhostTowerMode() override;

	void BuiltTowerMode() override;

	UFUNCTION()
		void Fire();

	UPROPERTY()
		float sphereSizeBuff;

public:

	void ChangeColour(bool canBuild) override;

	void SetTowerMode(bool isTowerBuilt) override;

	void TakeBuff(class UBuffComponent* buff) override;

	void SetAttackSpeed(float speed) override;

	void RemoveBuff() override;

	void UpgradeTower(float amount) override;
};
