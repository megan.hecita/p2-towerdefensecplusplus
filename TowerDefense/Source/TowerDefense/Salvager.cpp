// Fill out your copyright notice in the Description page of Project Settings.


#include "Salvager.h"
#include "Enemy.h"
#include "PlayerWallet.h"
#include "Components/SphereComponent.h"

ASalvager::ASalvager()
{
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ASalvager::LookForTarget);
}

void ASalvager::LookForTarget(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
		TargetInRangeArray.Add(enemy);	
}

void ASalvager::BeginPlay()
{
	Super::BeginPlay();
}

void ASalvager::DoAction()
{
}

void ASalvager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isBuilt)
	{
		SetTarget();

		currentTime += 1 * DeltaTime;

		if (currentTime >= ReceiveGoldTime)
		{
			GiveCoins();

			currentTime = 0;
		}
	}
}

void ASalvager::SetTarget()
{
	if (TargetInRangeArray.Num() > 0 && isBuilt)
	{
		AEnemy* curEnemy = Cast<AEnemy>(TargetInRangeArray[0]);

		curEnemy->OnDeath.AddDynamic(this, &ASalvager::GiveCoins);

		TargetInRangeArray.Remove(curEnemy);
	}
}

void ASalvager::GhostTowerMode()
{
	Super::GhostTowerMode();
}

void ASalvager::BuiltTowerMode()
{
	Super::BuiltTowerMode();
}

void ASalvager::GiveCoins()
{
	wallet->ReceiveGold(effectValue);
}

void ASalvager::ChangeColour(bool canBuild)
{
	Super::ChangeColour(canBuild);
}

void ASalvager::SetTowerMode(bool isTowerBuilt)
{
	Super::SetTowerMode(isTowerBuilt);
}

void ASalvager::UpgradeTower(float amount)
{
	Super::UpgradeTower(amount);
}
