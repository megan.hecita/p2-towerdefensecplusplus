// Fill out your copyright notice in the Description page of Project Settings.


#include "MissileTower.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "Enemy.h"
#include "Projectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "BuffComponent.h"

AMissileTower::AMissileTower()
{
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMeshStand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshStand"));
	StaticMeshStand->SetupAttachment(RootComponent);
	StaticMeshStand->bEditableWhenInherited = true;

	StaticMeshTurret = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshTurret"));
	StaticMeshTurret->SetupAttachment(StaticMeshStand);
	StaticMeshTurret->bEditableWhenInherited = true;

	ProjectilePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ProjectilePoint->SetupAttachment(StaticMeshTurret);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AMissileTower::LookForTarget);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &AMissileTower::ChangeTarget);
}

void AMissileTower::LookForTarget(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		if (!enemy->isFlying)
			TargetInRangeArray.Add(enemy);
	}
}

void AMissileTower::ChangeTarget(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		target = nullptr;

		TargetInRangeArray.Remove(enemy);

		SetTarget();
	}
}

void AMissileTower::DoAction()
{
	Fire();
}

void AMissileTower::RotateToTarget()
{
	FRotator Rot = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), target->GetActorLocation());

	StaticMeshTurret->SetWorldRotation(Rot);

	DoAction();
}

void AMissileTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMissileTower::SetTarget()
{
	if (TargetInRangeArray.Num() > 0 && isBuilt)
	{
		target = Cast<AEnemy>(TargetInRangeArray[0]);

		RotateToTarget();
	}
}

void AMissileTower::GhostTowerMode()
{
	Super::GhostTowerMode();
}

void AMissileTower::BuiltTowerMode()
{
	Super::BuiltTowerMode();

	if (!target)
		SetTarget();

	else
		RotateToTarget();
}

void AMissileTower::Fire()
{
	if (target && canFire)
	{
		canFire = false;

		AProjectile* spawnedProjectile = GetWorld()->SpawnActor<AProjectile>(Projectile, ProjectilePoint->GetComponentTransform());

		spawnedProjectile->UpgradeSphereRadius(sphereSizeBuff);

		spawnedProjectile->SetDamage(effectValue);

		curTime = 0;
	}
}

void AMissileTower::ChangeColour(bool canBuild)
{
	Super::ChangeColour(canBuild);
}

void AMissileTower::SetTowerMode(bool isTowerBuilt)
{
	Super::SetTowerMode(isTowerBuilt);
}

void AMissileTower::TakeBuff(UBuffComponent* buff)
{
	Super::TakeBuff(buff);
}

void AMissileTower::SetAttackSpeed(float speed)
{
	Super::SetAttackSpeed(speed);
}

void AMissileTower::RemoveBuff()
{
	Super::RemoveBuff();
}

void AMissileTower::UpgradeTower(float amount)
{
	Super::UpgradeTower(amount);
	
	sphereSizeBuff += 200;
}
