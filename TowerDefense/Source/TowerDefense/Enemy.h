// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeath);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKilled, int32, Amount);

UCLASS()
class TOWERDEFENSE_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:

	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY()
		FDeath OnDeath;

	UPROPERTY()
		FKilled OnKilled;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		float baseHP;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		int baseReward;
	
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		int CurrentTarget;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variables")
		class UHealthComponent* health;
	
public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		float damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		float speed;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AWaypoint*> WaypointArray;

	UPROPERTY()
		bool isBurning;

	UFUNCTION()
		void MoveToWaypoints();

	UFUNCTION()
		void SetPath(TArray<class AWaypoint*> path, int32 starting);

	UFUNCTION()
		void SetHealthAndReward(float hpBuff, int goldBuff);
	
	UFUNCTION()
		void TakeDamage(float damageValue);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Variables")
		int deathReward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isFlying;

	UFUNCTION()
		void TakeBuff(class UBuffComponent* buff);

	UFUNCTION()
		void ModifySpeed(float newSpeed);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ChangeSpeed();

	UFUNCTION()
		void RemoveBuff(TSubclassOf<class UBuffComponent> buff);

private:

	UFUNCTION()
		void Die(AActor* actor);

	UPROPERTY()
		TArray<class UBuffComponent*> curBuffsArray;

};
