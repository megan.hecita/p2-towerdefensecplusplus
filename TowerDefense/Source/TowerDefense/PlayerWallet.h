// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerWallet.generated.h"

UCLASS()
class TOWERDEFENSE_API APlayerWallet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerWallet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite)
		float gold;

	UFUNCTION()
		void ReceiveGold(int32 amount);

	UFUNCTION()
		void DecreaseGold(int32 amount);

};
