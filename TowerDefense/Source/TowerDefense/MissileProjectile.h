// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "MissileProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMissileProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:

	AMissileProjectile();

private:

	UFUNCTION()
		void DoDamage(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void DestroySelf() override;

	UFUNCTION()
		void OnHitTrigger(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:

	virtual void Tick(float DeltaTime) override;

	void SetDamage(int amount) override;

	void UpgradeSphereRadius(float upSize) override;
};
