// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSpawned);

UCLASS()
class TOWERDEFENSE_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int pathNum;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerWallet* playerWallet;
	
private:

	UPROPERTY()
		int32 startingPoint;

	UFUNCTION()
		TArray<class AWaypoint*> GetRandomPath();

	UPROPERTY()
		float hpBuff;

	UPROPERTY()
		int goldBuff;

public:	

	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UWaveInfo* WaveArray;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AWaypoint*> WaypointPathA;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AWaypoint*> WaypointPathB;

	UFUNCTION()
		void SpawnEnemies(int32 index);

	UFUNCTION()
		void AddBuff();

	UPROPERTY(BlueprintAssignable)
		FOnSpawned OnSpawned;
};
