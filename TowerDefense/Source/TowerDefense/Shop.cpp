// Fill out your copyright notice in the Description page of Project Settings.


#include "Shop.h"
#include "PlayerWallet.h"
#include "Tower.h"
#include "TowerNode.h"
#include "TowerList.h"

// Sets default values
AShop::AShop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShop::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < NodeArray.Num(); i++)
	{
		ATowerNode* curNode = Cast<ATowerNode>(NodeArray[i]);

		curNode->OnTNClicked.AddDynamic(this, &AShop::SetTowerNode);
	}
}

void AShop::PurchaseTower()
{
	if (canBuild && chosenTower)
	{
		chosenTower->SetTowerMode(true);

		chosenNode->isOccupied = true;
		chosenNode->SetTowerPlaced(chosenTower);

		playerWallet->DecreaseGold(chosenTower->price);

		chosenNode->AddGoldSpent(chosenTower->price);
	}

	HideShopUI();
}

int32 AShop::ShowTowerPrice(int32 index)
{
	return int32(TowerList->TowerArrayPrice[index]);
}

int32 AShop::ShowTowerLevel()
{
	return int32(chosenNode->towerPlaced->level);
}

int32 AShop::ShowUpgradePrice()
{
	upgradePrice = chosenNode->towerPlaced->price / 2;

	return int32(upgradePrice);
}

void AShop::UpgradeTower()
{
	if (playerWallet->gold >= upgradePrice && chosenNode->towerPlaced->level < 4)
	{
		if (!chosenNode->towerPlaced->isBuffingTower)
			chosenNode->towerPlaced->UpgradeTower(ProjectileTowerBuff);

		else
			chosenNode->towerPlaced->UpgradeTower(BuffingTowerBuff);

		chosenNode->AddGoldSpent(upgradePrice);
		chosenNode->towerPlaced->UpgradePrice();
		playerWallet->DecreaseGold(upgradePrice);
		ChangeMeshColour();
	}
}

int32 AShop::ShowSellPrice()
{
	sellPrice = chosenNode->totalGoldSpent / 2;

	return int32(sellPrice);
}
                                                                                               
void AShop::SellTower()
{
	chosenNode->RemoveTower();
	playerWallet->ReceiveGold(sellPrice);
}

void AShop::SetTowerNode(ATowerNode* Tnode)
{
	chosenNode = Tnode;
}

void AShop::CheckIfBought()
{
	if (chosenTower)
	{
		bool isPurchased = chosenTower->isBuilt;

		if (!isPurchased)
		{
			chosenTower->Destroy();
			chosenNode->isOccupied = false;
		}
	}

	else
		chosenNode->isOccupied = false;
}

void AShop::ChangeMeshColour()
{
	if (chosenNode->towerPlaced->level == 2)
		chosenNode->towerPlaced->ChangeColour(TowerList->MaterialArray[0]);

	else if (chosenNode->towerPlaced->level == 3)
		chosenNode->towerPlaced->ChangeColour(TowerList->MaterialArray[1]);

	else if (chosenNode->towerPlaced->level >= 4)
		chosenNode->towerPlaced->ChangeColour(TowerList->MaterialArray[2]);
}

// Called every frame
void AShop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AShop::ShowTower(int32 index)
{
	if (chosenNode->isOccupied)
	{
		ATower* curTower = chosenNode->towerPlaced;

		curTower->Destroy();
	}
	
	chosenTower = GetWorld()->SpawnActor<ATower>(TowerList->TowerArray[index], chosenNode->GetSpawnLocation());
	
	chosenNode->SetTowerPlaced(chosenTower);

	chosenNode->isOccupied = true;

	CheckCanBuild();
}

void AShop::CheckCanBuild()
{
	if (playerWallet->gold >= chosenTower->price)
		canBuild = true;

	else
		canBuild = false;

	chosenTower->ChangeColour(canBuild);
}

