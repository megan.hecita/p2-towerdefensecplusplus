// Fill out your copyright notice in the Description page of Project Settings.


#include "StandardTurret.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "Enemy.h"
#include "Projectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "BuffComponent.h"

AStandardTurret::AStandardTurret()
{
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMeshBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshBase"));
	StaticMeshBase->SetupAttachment(RootComponent);
	StaticMeshBase->bEditableWhenInherited = true;

	StaticMeshPylon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshPylon"));
	StaticMeshPylon->SetupAttachment(StaticMeshBase);
	StaticMeshPylon->bEditableWhenInherited = true;

	StaticMeshTurret = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshTurret"));
	StaticMeshTurret->SetupAttachment(StaticMeshPylon);
	StaticMeshTurret->bEditableWhenInherited = true;

	StaticMeshGun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshGun"));
	StaticMeshGun->SetupAttachment(StaticMeshTurret);
	StaticMeshGun->bEditableWhenInherited = true;

	ProjectilePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ProjectilePoint->SetupAttachment(StaticMeshGun);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AStandardTurret::LookForTarget);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &AStandardTurret::ChangeTarget);
}

void AStandardTurret::LookForTarget(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
		TargetInRangeArray.Add(enemy);
}

void AStandardTurret::ChangeTarget(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		target = nullptr;

		TargetInRangeArray.Remove(enemy);

		SetTarget();
	}
}

void AStandardTurret::DoAction()
{
	Fire();	
}

void AStandardTurret::RotateToTarget()
{
	FRotator Rot = UKismetMathLibrary::FindLookAtRotation(target->GetActorLocation(), this->GetActorLocation());
	
	StaticMeshTurret->SetWorldRotation(Rot);

	DoAction();
}

void AStandardTurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AStandardTurret::SetTarget()
{
	if (TargetInRangeArray.Num() > 0 && isBuilt)
	{
		target = Cast<AEnemy>(TargetInRangeArray[0]);

		RotateToTarget();
	}
}

void AStandardTurret::ChangeColour(bool canBuild)
{
	Super::ChangeColour(canBuild);
}

void AStandardTurret::SetTowerMode(bool isTowerBuilt)
{
	Super::SetTowerMode(isTowerBuilt);
}

void AStandardTurret::TakeBuff(UBuffComponent* buff)
{
	Super::TakeBuff(buff);
}

void AStandardTurret::SetAttackSpeed(float speed)
{
	Super::SetAttackSpeed(speed);
}

void AStandardTurret::RemoveBuff()
{
	Super::RemoveBuff();
}

void AStandardTurret::UpgradeTower(float amount)
{
	Super::UpgradeTower(amount);

	attackSpeed -= 1;

	if (attackSpeed <= 0)
		attackSpeed = 1;
}

void AStandardTurret::GhostTowerMode()
{
	Super::GhostTowerMode();
}

void AStandardTurret::BuiltTowerMode()
{
	Super::BuiltTowerMode();

	if (!target)
		SetTarget();

	else
		RotateToTarget();
}

void AStandardTurret::Fire()
{
	if (target && canFire)
	{
		canFire = false;

		AProjectile* spawnedProjectile = GetWorld()->SpawnActor<AProjectile>(Projectile, ProjectilePoint->GetComponentTransform());
		
		spawnedProjectile->SetDamage(effectValue);

		curTime = 0;
	}
}
