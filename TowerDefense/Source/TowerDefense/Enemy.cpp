// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "EnemyController.h"
#include "Waypoint.h"
#include "Engine/StaticMesh.h"
#include "HealthComponent.h"
#include "PlayerWallet.h"
#include "BuffComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;

	health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));

	OnDestroyed.AddDynamic(this, &AEnemy::Die);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnemy::MoveToWaypoints()
{
	AEnemyController* EnemyController = Cast<AEnemyController>(GetController());

	for (AActor* currentTarget : WaypointArray)
	{
		AWaypoint* nextWaypoint = Cast<AWaypoint>(currentTarget);

		if (nextWaypoint)
		{
			
			if (nextWaypoint->WaypointOrder == CurrentTarget)
			{
				EnemyController->MoveToActor(nextWaypoint, 5.f, false);

				CurrentTarget++;

				break;
			}
		}
	}
}

void AEnemy::SetPath(TArray<class AWaypoint*> path, int32 startingPoint)
{
	CurrentTarget = startingPoint;

	for (int i = 0; i < path.Num(); i++)
	{
		WaypointArray[i] = path[i];
	}

	MoveToWaypoints();
}

void AEnemy::SetHealthAndReward(float hpBuff, int goldBuff)
{
	baseHP += hpBuff;
	
	deathReward = (baseReward + goldBuff);
	
	if (health)
		health->setHealth(baseHP);
}

void AEnemy::TakeBuff(UBuffComponent* buff)
{
	UBuffComponent* curBuff = Cast<UBuffComponent>(buff);
	curBuff->ApplyBuff();

	curBuffsArray.Add(curBuff);
}

void AEnemy::RemoveBuff(TSubclassOf<class UBuffComponent> buff)
{
	if (curBuffsArray.Num() > 0)
	{
		for (UBuffComponent* curBuff : curBuffsArray)
		{
			if (curBuff->GetClass() == buff)
				curBuff->RemoveBuff();
		}
	}
}

void AEnemy::Die(AActor* actor)
{
	OnDeath.Broadcast();
}

void AEnemy::ModifySpeed(float newSpeed)
{
	speed = newSpeed;

	ChangeSpeed();
}

void AEnemy::TakeDamage(float damageValue)
{
	if (health)
	{
		health->DecreaseHealth(damageValue);

		if (health->hpValue == 0)
		{
			if (curBuffsArray.Num() > 0)
			{
				for (UBuffComponent* curBuff : curBuffsArray)
				{
					curBuff->RemoveBuff();
				}
			}

			OnKilled.Broadcast(deathReward);
			this->Destroy();
		}
	}
}