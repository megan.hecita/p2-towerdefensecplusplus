// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

UCLASS()
class TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* ProjectilePoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		class USphereComponent* SphereCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AProjectile> Projectile;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class UBuffComponent> Buff;

	UPROPERTY()
		AActor* target;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float effectValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool canFire;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float curTime;

	UPROPERTY()
		TArray<AActor*> TargetInRangeArray;

	UPROPERTY()
		class UBuffComponent* curBuff;

	virtual void RotateToTarget();

	virtual void DoAction();

	virtual void SetTarget();

	virtual void GhostTowerMode();

	virtual void BuiltTowerMode();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite)
		float price;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isBuilt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float attackSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isBuffingTower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int level;

	virtual void ChangeColour(bool canBuild);

	virtual void SetTowerMode(bool isTowerBuilt);

	virtual void TakeBuff(class UBuffComponent* buff);

	virtual void SetAttackSpeed(float speed);

	virtual void RemoveBuff();

	virtual void UpgradeTower(float amount);

	virtual void UpgradePrice();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ChangeColour(UMaterial* colour);
};
