// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserTower.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "Enemy.h"
#include "Projectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "BuffComponent.h"

ALaserTower::ALaserTower()
{
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMeshStand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshStand"));
	StaticMeshStand->SetupAttachment(RootComponent);
	StaticMeshStand->bEditableWhenInherited = true;

	StaticMeshTurret = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshTurret"));
	StaticMeshTurret->SetupAttachment(StaticMeshStand);
	StaticMeshTurret->bEditableWhenInherited = true;

	ProjectilePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ProjectilePoint->SetupAttachment(StaticMeshTurret);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ALaserTower::LookForTarget);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &ALaserTower::ChangeTarget);
}

void ALaserTower::LookForTarget(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
		TargetInRangeArray.Add(enemy);
}

void ALaserTower::ChangeTarget(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		target = nullptr;

		TargetInRangeArray.Remove(enemy);

		SetTarget();
	}
}

void ALaserTower::DoAction()
{
	Fire();
}

void ALaserTower::RotateToTarget()
{
	FRotator Rot = UKismetMathLibrary::FindLookAtRotation(target->GetActorLocation(), this->GetActorLocation());

	StaticMeshTurret->SetWorldRotation(Rot);

	DoAction();
}

void ALaserTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALaserTower::SetTarget()
{
	if (TargetInRangeArray.Num() > 0 && isBuilt)
	{
		for (int i = 0; i < TargetInRangeArray.Num(); i++)
		{
			AEnemy* curEnemy = Cast<AEnemy>(TargetInRangeArray[i]);

			if (!curEnemy->isBurning)
			{
				target = Cast<AEnemy>(TargetInRangeArray[i]);
				RotateToTarget();
			}
		}
	}
}

void ALaserTower::GhostTowerMode()
{
	Super::GhostTowerMode();
}

void ALaserTower::BuiltTowerMode()
{
	Super::BuiltTowerMode();

	if (!target)
		SetTarget();

	else RotateToTarget();
}

void ALaserTower::Fire()
{
	if (target && canFire)
	{
		canFire = false;

		UBuffComponent* BuffEffect = NewObject<UBuffComponent>(target, Buff, "BurnEffect");
		this->AddInstanceComponent(BuffEffect);
		BuffEffect->RegisterComponent();

		BuffEffect->ModifyBuffEffectValue(effectValue);
		BuffEffect->ModifyDuration(buffTimer);

		AProjectile* spawnedProjectile = GetWorld()->SpawnActor<AProjectile>(Projectile, ProjectilePoint->GetComponentTransform());

		spawnedProjectile->SetBuff(BuffEffect);

		SetTarget();

		curTime = 0;
	}
}

void ALaserTower::ChangeColour(bool canBuild)
{
	Super::ChangeColour(canBuild);
}

void ALaserTower::SetTowerMode(bool isTowerBuilt)
{
	Super::SetTowerMode(isTowerBuilt);
}

void ALaserTower::TakeBuff(UBuffComponent* buff)
{
	Super::TakeBuff(buff);
}

void ALaserTower::SetAttackSpeed(float speed)
{
	Super::SetAttackSpeed(speed);
}

void ALaserTower::RemoveBuff()
{
	Super::RemoveBuff();
}

void ALaserTower::UpgradeTower(float amount)
{
	Super::UpgradeTower(amount);
	buffTimer += 2;
}
