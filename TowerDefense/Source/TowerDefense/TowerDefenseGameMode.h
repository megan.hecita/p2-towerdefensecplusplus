// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseGameMode.generated.h"

UCLASS(minimalapi)
class ATowerDefenseGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATowerDefenseGameMode();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AEnemySpawner*> SpawnerArray;

	UFUNCTION(BlueprintCallable)
		void SpawnEnemies();

private:

	FTimerHandle TimerHand;

	FTimerDelegate TimerDel;

	UFUNCTION()
		int32 CheckWave();

	UFUNCTION()
		void StartNewWave();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		int32 baseReward;

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 currentWave;

	UPROPERTY()
		int32 currentEnemySpawned;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 totalEnemiesPerWave;

	UPROPERTY(BlueprintReadOnly)
		int32 enemiesDestroyed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerWallet* playerWallet;

	UFUNCTION()
		void AddEnemyDestroyed();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ShowWinScreen();

};

