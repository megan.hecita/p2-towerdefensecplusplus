// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerNode.h"
#include "Components/StaticMeshComponent.h"
#include "Tower.h"
#include "Components/ArrowComponent.h"

// Sets default values
ATowerNode::ATowerNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;
	StaticMesh->OnClicked.AddDynamic(this, &ATowerNode::OnNodeClicked);

	Arrow = CreateDefaultSubobject<UArrowComponent>("Arrow");
	Arrow->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATowerNode::BeginPlay()
{
	Super::BeginPlay();
}

void ATowerNode::OnNodeClicked(UPrimitiveComponent* ClickedComp, FKey ButtonPressed)
{
	if (!isOccupied)
	{
		ShowShopUI();
		OnTNClicked.Broadcast(this);
	}

	else
	{
		ShowTowerUI();
		OnTNClicked.Broadcast(this);
	}
}

// Called every frame
void ATowerNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FTransform ATowerNode::GetSpawnLocation()
{
	return FTransform(Arrow->GetComponentTransform());
}

void ATowerNode::SetTowerPlaced(ATower* tower)
{
	towerPlaced = tower;
}

void ATowerNode::AddGoldSpent(float amount)
{
	totalGoldSpent += amount;
}

void ATowerNode::RemoveTower()
{
	towerPlaced->Destroy();
	isOccupied = false;
	totalGoldSpent = 0;
}

