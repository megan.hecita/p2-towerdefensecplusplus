// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerGenerator.h"
#include "Components/SphereComponent.h"
#include "Tower.h"
#include "Kismet/KismetMathLibrary.h"
#include "BuffComponent.h"

APowerGenerator::APowerGenerator()
{
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &APowerGenerator::StartBuff);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &APowerGenerator::EndBuff);

	this->OnDestroyed.AddDynamic(this, &APowerGenerator::OnRemoveTower);
}

void APowerGenerator::ChangeColour(bool canBuild)
{
	Super::ChangeColour(canBuild);
}

void APowerGenerator::SetTowerMode(bool isTowerBuilt)
{
	Super::SetTowerMode(isTowerBuilt);
}

void APowerGenerator::OnRemoveTower(AActor* actor)
{
	if (TargetInRangeArray.Num() > 0)
	{
		for (int i = 0; i < TargetInRangeArray.Num(); i++)
		{
			ATower* curTower = Cast<ATower>(TargetInRangeArray[i]);

			curTower->RemoveBuff();
		}
	}
}

void APowerGenerator::UpgradeTower(float amount)
{
	Super::UpgradeTower(amount);
}

void APowerGenerator::StartBuff(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ATower* towerTarget = Cast<ATower>(OtherActor))
	{
		if (!towerTarget->isBuffingTower)
		{
			TargetInRangeArray.Add(towerTarget);

			UBuffComponent* BuffEffect = NewObject<UBuffComponent>(towerTarget, Buff, "Effect");
			this->AddInstanceComponent(BuffEffect);
			BuffEffect->RegisterComponent();

			BuffEffect->ModifyBuffEffectValue(effectValue);

			BuffEffect->SetTarget(towerTarget);

			towerTarget->TakeBuff(BuffEffect);
		}
	}
}

void APowerGenerator::EndBuff(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (ATower* towerTarget = Cast<ATower>(OtherActor))
	{
		if (!towerTarget->isBuffingTower)
		{
			TargetInRangeArray.Remove(towerTarget);

			towerTarget->RemoveBuff();
		}
	}
}

void APowerGenerator::DoAction()
{
}

void APowerGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APowerGenerator::SetTarget()
{
}

void APowerGenerator::GhostTowerMode()
{
	Super::GhostTowerMode();
	SphereCollision->SetGenerateOverlapEvents(false);
}

void APowerGenerator::BuiltTowerMode()
{
	Super::BuiltTowerMode();
	SphereCollision->SetGenerateOverlapEvents(true);
}
