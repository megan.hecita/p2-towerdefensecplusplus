// Fill out your copyright notice in the Description page of Project Settings.


#include "BurnComponent.h"
#include "Enemy.h"

UBurnComponent::UBurnComponent()
{
	
}

void UBurnComponent::BeginPlay()
{
	Super::BeginPlay();

	BuffEffectValue = 0;
	duration = 5;

	TimerDel.BindUFunction(this, FName("TriggerBurnTimer"));
}

void UBurnComponent::TriggerBurnTimer()
{
	if (BuffTarget)
	{
		if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
		{
			enemy->TakeDamage(BuffEffectValue);
			enemy->isBurning = true;
		}
		GetWorld()->GetTimerManager().SetTimer(TimerHand, TimerDel, 1.f, false);
	}
}

void UBurnComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	curTime += 1 * DeltaTime;

	if (curTime >= duration)
		RemoveBuff();
}

void UBurnComponent::SetTarget(AActor* target)
{
	Super::SetTarget(target);
}

void UBurnComponent::ApplyBuff()
{
	if (BuffTarget)
	{
		TriggerBurnTimer();

		curTime = 0;
	}
}

void UBurnComponent::RemoveBuff()
{
	if (AEnemy* enemy = Cast<AEnemy>(BuffTarget))
		enemy->isBurning = false;

	Super::RemoveBuff();
}

void UBurnComponent::ModifyBuffEffectValue(float amount)
{
	Super::ModifyBuffEffectValue(amount);
}

void UBurnComponent::ModifyDuration(float amount)
{
	Super::ModifyDuration(amount);
}
