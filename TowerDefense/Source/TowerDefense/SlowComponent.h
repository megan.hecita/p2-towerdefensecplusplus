// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "SlowComponent.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API USlowComponent : public UBuffComponent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	USlowComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:

	UPROPERTY()
		float originalSpeed;

	UPROPERTY()
		float slowSpeed;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetTarget(AActor* target) override;

	void ApplyBuff() override;

	void RemoveBuff() override;

	void ModifyBuffEffectValue(float amount) override;

};
