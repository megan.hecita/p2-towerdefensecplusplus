// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTowerNodeClicked, class ATowerNode*, Node);
 
UCLASS()
class TOWERDEFENSE_API ATowerNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerNode();

	UPROPERTY()
		FTowerNodeClicked OnTNClicked;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* Arrow;

	UFUNCTION()
		void OnNodeClicked(UPrimitiveComponent* ClickedComp, FKey ButtonPressed);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ShowShopUI();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ShowTowerUI();

private:

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isOccupied;

	UFUNCTION()
		FTransform GetSpawnLocation();

	UFUNCTION()
		void SetTowerPlaced(class ATower* tower);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class ATower* towerPlaced;

	UPROPERTY()
		float totalGoldSpent;

	UFUNCTION()
		void AddGoldSpent(float amount);

	UFUNCTION()
		void RemoveTower();
};
