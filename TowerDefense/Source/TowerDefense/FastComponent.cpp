// Fill out your copyright notice in the Description page of Project Settings.


#include "FastComponent.h"
#include "Tower.h"

UFastComponent::UFastComponent()
{

}

void UFastComponent::BeginPlay()
{
	Super::BeginPlay();

	BuffEffectValue = 0;
}

void UFastComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{

}

void UFastComponent::SetTarget(AActor* target)
{
	Super::SetTarget(target);

	if (ATower* tower = Cast<ATower>(BuffTarget))
	{
		originalSpeed = tower->attackSpeed;

		fastSpeed = tower->attackSpeed - BuffEffectValue;
	}
}

void UFastComponent::ApplyBuff()
{
	if (ATower* tower = Cast<ATower>(BuffTarget))
		tower->SetAttackSpeed(fastSpeed);
}

void UFastComponent::RemoveBuff()
{
	if (ATower* tower = Cast<ATower>(BuffTarget))
		tower->SetAttackSpeed(originalSpeed + 1);
}

void UFastComponent::ModifyBuffEffectValue(float amount)
{
	Super::ModifyBuffEffectValue(amount);
}
