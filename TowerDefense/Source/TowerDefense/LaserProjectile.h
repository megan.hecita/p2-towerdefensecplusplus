// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "LaserProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ALaserProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:

	ALaserProjectile();

private:

	UFUNCTION()
		void DoDamage(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void DestroySelf() override;

public:

	virtual void Tick(float DeltaTime) override;

	void SetBuff(class UBuffComponent* Buff) override;
};
