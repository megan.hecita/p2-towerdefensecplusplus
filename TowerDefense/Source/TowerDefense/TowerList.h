// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerList.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UTowerList : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<TSubclassOf<class ATower>> TowerArray;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int32> TowerArrayPrice;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class UMaterial*> MaterialArray;
};
