// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserProjectile.h"
#include "Enemy.h"
#include "Components/SphereComponent.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "BuffComponent.h"

ALaserProjectile::ALaserProjectile()
{
	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	AddOwnedComponent(ProjectileMovement);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ALaserProjectile::DoDamage);
}
void ALaserProjectile::DoDamage(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		BuffEffect->SetTarget(target);

		target->TakeBuff(BuffEffect);

		DestroySelf();
	}
}

void ALaserProjectile::DestroySelf()
{
	Super::DestroySelf();
}

void ALaserProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALaserProjectile::SetBuff(UBuffComponent* Buff)
{
	BuffEffect = Buff;
}
