// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"


UCLASS()
class TOWERDEFENSE_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		float lifetime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		bool isFlyingIgnored;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USphereComponent* SphereCollision;

	UPROPERTY()
		TArray<AActor*> TargetInRangeArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float curTime;
	
	virtual void DestroySelf();

	UPROPERTY()
		float damage;

	UPROPERTY()
		class UBuffComponent* BuffEffect;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void SetDamage(int amount);

	virtual void SetBuff(class UBuffComponent* Buff);

	virtual void UpgradeSphereRadius(float upSize);
};
