// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shop.generated.h"

UCLASS()
class TOWERDEFENSE_API AShop : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShop();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UTowerList* TowerList;
	
	UPROPERTY()
		class ATower* chosenTower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool canBuild;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerWallet* playerWallet;

	UFUNCTION()
		void SetTowerNode(class ATowerNode* Tnode);

	UPROPERTY(EditAnywhere, BlueprintReadOnly);
		class ATowerNode* chosenNode;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void HideShopUI();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void HideTowerUI();

	UFUNCTION(BlueprintCallable)
		void CheckIfBought();

	UFUNCTION()
		void ChangeMeshColour();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ProjectileTowerBuff;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int BuffingTowerBuff;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
		int32 upgradePrice;

	UPROPERTY()
		int32 sellPrice;

	UFUNCTION(BlueprintCallable)
		void ShowTower(int32 index);

	UFUNCTION()
		void CheckCanBuild();

	UFUNCTION(BlueprintCallable)
		void PurchaseTower();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ATowerNode*> NodeArray;

	UFUNCTION(BlueprintCallable)
		int32 ShowTowerPrice(int32 index);

	UFUNCTION(BlueprintCallable)
		int32 ShowTowerLevel();

	UFUNCTION(BlueprintCallable)
		int32 ShowUpgradePrice();

	UFUNCTION(BlueprintCallable)
		void UpgradeTower();

	UFUNCTION(BlueprintCallable)
		int32 ShowSellPrice();

	UFUNCTION(BlueprintCallable)
		void SellTower();
};
