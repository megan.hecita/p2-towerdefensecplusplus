// Fill out your copyright notice in the Description page of Project Settings.


#include "EMP.h"
#include "Components/SphereComponent.h"
#include "Enemy.h"
#include "Kismet/KismetMathLibrary.h"
#include "BuffComponent.h"

AEMP::AEMP()
{
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AEMP::StartBuff);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &AEMP::EndBuff);

	this->OnDestroyed.AddDynamic(this, &AEMP::OnRemoveTower);
}

void AEMP::ChangeColour(bool canBuild)
{
	Super::ChangeColour(canBuild);
}

void AEMP::SetTowerMode(bool isTowerBuilt)
{
	Super::SetTowerMode(isTowerBuilt);
}

void AEMP::OnRemoveTower(AActor* actor)
{
	if (TargetInRangeArray.Num() > 0)
	{
		for (int i = 0; i < TargetInRangeArray.Num(); i++)
		{
			AEnemy* curEnemy = Cast<AEnemy>(TargetInRangeArray[i]);

			curEnemy->RemoveBuff(Buff);
		}
	}
}

void AEMP::UpgradeTower(float amount)
{
	Super::UpgradeTower(amount);

	float originalRad = SphereCollision->GetScaledSphereRadius();

	float newRad = originalRad + 200;

	SphereCollision->SetSphereRadius(newRad);
}

void AEMP::StartBuff(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemyTarget = Cast<AEnemy>(OtherActor))
	{
		TargetInRangeArray.Add(enemyTarget);

		UBuffComponent* BuffEffect = NewObject<UBuffComponent>(enemyTarget, Buff, "SlowEffect");
		this->AddInstanceComponent(BuffEffect);
		BuffEffect->RegisterComponent();

		BuffEffect->ModifyBuffEffectValue(effectValue);
		
		BuffEffect->SetTarget(enemyTarget);

		enemyTarget->TakeBuff(BuffEffect);
	}
}

void AEMP::EndBuff(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* enemyTarget = Cast<AEnemy>(OtherActor))
	{
		TargetInRangeArray.Remove(enemyTarget);
		enemyTarget->RemoveBuff(Buff);
	}
}

void AEMP::DoAction()
{
}

void AEMP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEMP::SetTarget()
{
}

void AEMP::GhostTowerMode()
{
	Super::GhostTowerMode();
	SphereCollision->SetGenerateOverlapEvents(false);
}

void AEMP::BuiltTowerMode()
{
	Super::BuiltTowerMode();
	SphereCollision->SetGenerateOverlapEvents(true);
}
