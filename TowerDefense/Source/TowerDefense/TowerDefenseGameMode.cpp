// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefenseGameMode.h"
#include "TowerDefenseCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "EnemySpawner.h"
#include "Kismet/KismetMathLibrary.h"
#include "Enemy.h"
#include "PlayerWallet.h"

ATowerDefenseGameMode::ATowerDefenseGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	enemiesDestroyed = 0;
}

int32 ATowerDefenseGameMode::CheckWave()
{
	int32 index;

	if (currentWave < 10)
		index = UKismetMathLibrary::RandomIntegerInRange(0, 1);

	else
		index = 2;

	return index;
}

void ATowerDefenseGameMode::StartNewWave()
{
	enemiesDestroyed = 0;
	currentEnemySpawned = 0;
	currentWave += 1;
	baseReward += 100;
	playerWallet->ReceiveGold(baseReward);

	for (int i = 0; i < SpawnerArray.Num(); i++)
	{
		SpawnerArray[i]->AddBuff();
	}

	if (currentWave == 10)
		totalEnemiesPerWave = 4;

	if (currentWave <= 10)
		GetWorld()->GetTimerManager().SetTimer(TimerHand, TimerDel, 10.f, false);

	else if (currentWave > 10)
		ShowWinScreen();
}

void ATowerDefenseGameMode::AddEnemyDestroyed()
{
	enemiesDestroyed += 1;
}

void ATowerDefenseGameMode::Tick(float DeltaTime)
{
	if (enemiesDestroyed == totalEnemiesPerWave)
		StartNewWave();
}

void ATowerDefenseGameMode::SpawnEnemies()
{
	if (currentEnemySpawned < totalEnemiesPerWave)
	{
		for (int i = 0; i < SpawnerArray.Num(); i++)
		{
			SpawnerArray[i]->SpawnEnemies(CheckWave());
			currentEnemySpawned++;
		}

		TimerDel.BindUFunction(this, FName("SpawnEnemies"));
		GetWorld()->GetTimerManager().SetTimer(TimerHand, TimerDel, 5.f, false);
	}
}


